import React, {Component} from 'react';
import './App.css';

class App extends Component {
  state = {
    data: [],
  };

  async componentDidMount() {
    const url = "https://bd4l0e9l0k.execute-api.eu-west-2.amazonaws.com/prod/";
    const response = await fetch(url);
    const apiData = await response.json();
    this.setState({data: apiData});
  }

  render() {
    let rentalNodes = this.state.data.map(rental => {
      return (
        <div className="card m-3">
          <img className="card-img-top" src={rental.PhotoSource} alt="photo"/>
          <div className="card-body">
            <h5 className="card-title">{rental.Street}</h5>
            <p className="card-text">{rental.City}</p>
            <p className="card-text">{rental.Rent}</p>
            <a className="btn btn-primary" href="#">Mark interest</a>
          </div>
        </div>
      )
    })
    return (
      <React.Fragment>
        <div className="container">
          <div className="col-md-12">
            <h3 className="row p-5 justify-content-around">Rentals</h3>
            <div className="d-flex">
              {rentalNodes}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
    }
}

export default App;
